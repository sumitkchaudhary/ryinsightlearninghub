package com.RYYoutube.JavaSeries.AccessModifier;

public class AccessModifierChild  extends AccessModifierParent{
	
	public static void main(String[] args) {
		AccessModifierParent referenceVariable=new AccessModifierParent();
		
	//	referenceVariable.defaultMethod();
		//referenceVariable.publicMethod();
	//	referenceVariable.privateMethod();
		referenceVariable.protectedMethod();
		
		AccessModifierChild obj=new AccessModifierChild();
		obj.protectedMethod();
		
		obj.defaultMethod();
		obj.publicMethod();
		
		
	}

}
