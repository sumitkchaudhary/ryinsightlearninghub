package com.RYYoutube.JavaSeries.AccessModifier;

public class AccessModifierParent {
	
/* Default --is visible only within the package (package private) 
 * Public - is visible to all/ everywhere
 * private -- is visible only within the class
 * protected- is visible only within the package or all subclasses
 * 
 * */
	void defaultMethod() {
		System.out.println("I'm a deafult method and can access me easily with the package.  ");
	}

	public void publicMethod() {
		System.out.println("I'm a public method and anybody can access me easily. Or I'm available for out class ");
	}
	
	private void privateMethod() {
		System.out.println("I'm a private method and can access me easily with the class only. ");
	}
	
	protected void protectedMethod() {
		System.out.println("I'm a protected method and anybody can access me easily out of class but with in the package. "
				+ "Or my subclasss can access me easily ");
	}
	final void finalMethod() {
		System.out.println("final");
	}
	
	final static int finalVariable=100;
	
	public static int publitIntVariable=10000;
	
	public static void main(String[] args) {
		//How to create an object of the class? ClassName referenceVariableName=new ClassName();
		AccessModifierParent referenceVariable=new AccessModifierParent();
		
		referenceVariable.defaultMethod();
		referenceVariable.publicMethod();
		referenceVariable.privateMethod();
		referenceVariable.protectedMethod();
		publitIntVariable=5000;
		//finalVariable=1000;
		
	}
	
}
