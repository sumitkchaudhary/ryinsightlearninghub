package com.RYYoutube.JavaSeries.AccessModifier;

import java.util.Scanner;

public class InputOutStreamConcept {
	
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Please enter the data");
		double  intVariable=sc.nextDouble();
		System.out.println(intVariable);
	}

}
