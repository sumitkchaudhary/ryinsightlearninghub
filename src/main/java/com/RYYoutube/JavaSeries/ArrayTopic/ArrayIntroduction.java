/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 27-Aug-2022
 *  FILE NAME  		: 	 ArrayIntroduction.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    7:20:17 pm
 */
package com.RYYoutube.JavaSeries.ArrayTopic;
public class ArrayIntroduction {
	public static void main(String[] args) {
		//1st way to declare an array
		String friendsNameList[]= {"Sumit","Sushil", "Sourav","Sachin","Narendra"};
		double d[]= {10.50, 40.15};
		
		
		for(String name: friendsNameList) {
			System.out.println(name);
		}
		
	}
}
