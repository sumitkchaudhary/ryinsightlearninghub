/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 11-Sep-2022
 *  FILE NAME  		: 	 ThreeD.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    1:09:53 am
 */
package com.RYYoutube.JavaSeries.ArrayTopic;
public class ThreeD {
	public static void main(String[] args) {
		String arryOfStrings[][][]= {
				{
					{"Sumit Kumar"},
					{"Sushil Kumar"},
					{"Sachin Kumar"}
				},
				{
					{"Saurav Kumar"},
					{"Harsh Kumar"},
					{"Pankaj Kumar"}
				},
				{
					{"Yogesh Kumar"},
					{"Rajkumar Kumar"},
					{"Deepak Kumar"}
				}
				
		};
		
		/*
		for(int sheetCount=0; sheetCount<arryOfStrings.length; sheetCount++) {
			for(int columnCount=0; columnCount<arryOfStrings[sheetCount].length; columnCount++) {
				for(int rowCount=0; rowCount<arryOfStrings[sheetCount][columnCount].length;rowCount++) {
					System.out.println("My Best friends:  "+arryOfStrings[sheetCount][columnCount][rowCount]);
				}
			}
		}*/
		for(String sheetCount[][]:arryOfStrings) {
			for(String columnCount[]:sheetCount) {
				for(String rowCount:columnCount) {
					System.out.println("My Best friends:  "+rowCount);
				}
			}
		}

		
	}

}
