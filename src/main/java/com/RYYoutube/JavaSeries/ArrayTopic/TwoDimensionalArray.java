/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 27-Aug-2022
 *  FILE NAME  		: 	 TwoDimensionalArray.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:04:00 pm
 */
package com.RYYoutube.JavaSeries.ArrayTopic;
public class TwoDimensionalArray {
	public static void main(String[] args) {
		String [][] number=new String [3][4];
		number[0][0]="Sumit";
		number[0][1]="Sushil";
		number[0][2]="Saurav";
		number[0][3]="Sachin";
		
		number[1][0]="Gaurav";
		number[1][1]="Sonu";
		number[1][2]="Narendra";
		number[1][3]="Satish";
		
		number[2][0]="Chandrakar";
		number[2][1]="Vikas";
		number[2][2]="Sunil";		
		number[2][3]="Ajay";
		
		
		for(int i=0; i<number.length; i++) {//raw
			for(int j=0; j<number[i].length; j++) {//column
				System.out.println("Add of element "+i+""+j+" "+number[i][j]);
			}
		}
	}

}
