/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 19-Nov-2022
 *  FILE NAME  		: 	 ArrayListVsLinkedList.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    10:44:31 am
 */
package com.RYYoutube.JavaSeries.CollectionFramework.CollectionInterface.ListInterface;


import java.util.LinkedList;

public class ArrayListVsLinkedList {
	public static void main(String[] args) {
		LinkedList<Integer> tableOfNumber=new LinkedList<Integer>();
		//We add the table of the number which given by the user
		int no=2;
		for(int i=1;i<=11; i++ ) {
			tableOfNumber.add(no*i);
		}
		
		for(int a: tableOfNumber) {
			System.out.println(a);
		}
		
		int removedElement=tableOfNumber.remove(10);
		System.out.println("Removed element is: "+ removedElement);
		
	}
}
