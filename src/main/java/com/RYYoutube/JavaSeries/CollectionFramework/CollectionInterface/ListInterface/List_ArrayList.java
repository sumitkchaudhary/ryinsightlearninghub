/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 13-Nov-2022
 *  FILE NAME  		: 	 List_ArrayList.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    7:40:30 pm
 */
package com.RYYoutube.JavaSeries.CollectionFramework.CollectionInterface.ListInterface;

import java.util.ArrayList;
import java.util.List;

public class List_ArrayList {
	public static void main(String[] args) {
		
		List<String> counting =new ArrayList<String>();
		
		counting.add("Ram");
		counting.add("Shyam");
		counting.add("Sita");
		counting.add("Gita");
		counting.add("Amit");
		counting.add("Sumit");
		counting.add("Sushil");
		counting.add("Ashok");
		counting.add("Vivek");
		counting.add("Arun");
		
		//System.out.println(counting);
		System.out.println("Before remove an element");
		for(int i=0; i<counting.size(); i++) {
			System.out.println(counting.get(i));
		}
		
		counting.remove(5);
		System.out.println("After remove an element");
		for(int i=0; i<counting.size(); i++) {
			System.out.println(counting.get(i));
		}
		System.out.println("After remove an element");
		
		for(String i: counting) {
			System.out.println(i);
		}
	}

}
