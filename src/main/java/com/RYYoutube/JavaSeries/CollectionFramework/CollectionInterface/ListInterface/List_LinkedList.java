/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 13-Nov-2022
 *  FILE NAME  		: 	 List_LinkedList.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:00:01 pm
 */
package com.RYYoutube.JavaSeries.CollectionFramework.CollectionInterface.ListInterface;
import java.util.LinkedList;
import java.util.List;

public class List_LinkedList {
	public static void main(String[] args) {
		List<String> counting =new LinkedList<String>();
		
		counting.add("Ram");
		counting.add("Shyam");
		counting.add("Sita");
		counting.add("Gita");
		counting.add("Amit");
		counting.add("Sumit");
		counting.add("Sushil");
		counting.add("Ashok");
		counting.add("Vivek");
		counting.add("Arun");
		
		for(String i: counting) {
			System.out.println(i);
		}
	}

}
