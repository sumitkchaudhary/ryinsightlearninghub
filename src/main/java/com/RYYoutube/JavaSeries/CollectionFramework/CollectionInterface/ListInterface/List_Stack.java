/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 13-Nov-2022
 *  FILE NAME  		: 	 List_Stack.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:02:53 pm
 */
package com.RYYoutube.JavaSeries.CollectionFramework.CollectionInterface.ListInterface;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;


public class List_Stack {
	public static void main(String[] args) {
		Stack<Integer> counting =new Stack<Integer>();
		
		counting.add(23);
		counting.add(24);
		counting.push(56);
		counting.push(76);
		counting.push(22);
		counting.push(23);
		counting.push(44);
		counting.push(11);
		counting.push(1);
		counting.push(34);
		
		
		Iterator<Integer> iterateAllElement=counting.iterator();
		while(iterateAllElement.hasNext()) {
			System.out.println(iterateAllElement.next());
		}
		
		//int i=counting.pop();
		//System.out.println("Toppest Value is: "+i);
		
		int peeked=counting.peek();
		System.out.println("Peeked"+peeked);
		
	}

}
