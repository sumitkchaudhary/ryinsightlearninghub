/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 19-Nov-2022
 *  FILE NAME  		: 	 Vector.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    11:46:05 am
 */
package com.RYYoutube.JavaSeries.CollectionFramework.CollectionInterface.ListInterface;

import java.util.Vector;

public class List_Vector {
	public static void main(String[] args) {
		Vector<Integer> v=new Vector<Integer>();
		
		v.add(25);
		v.add(2);
		v.add(30);
		v.add(34);
		v.add(55);
		
		for(int i: v) {
			System.out.println(i);
		}
		
		int removedElement=v.remove(3);
		System.out.println("Removed Element is: "+removedElement);
	}

}
