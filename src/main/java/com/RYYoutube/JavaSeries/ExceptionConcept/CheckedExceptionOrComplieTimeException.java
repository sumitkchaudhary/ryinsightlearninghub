/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 11-Sep-2022
 *  FILE NAME  		: 	 CheckedExceptionOrComplieTimeException.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:04:49 pm
 */
package com.RYYoutube.JavaSeries.ExceptionConcept;

import java.io.IOException;

public class CheckedExceptionOrComplieTimeException {
	
	public static void displayExcpetionError() throws IOException {
		
		throw new IOException();
		//System.out.println("message");
	}
	
	public static void main(String[] args) throws IOException {
		displayExcpetionError();
	}
}
