/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 11-Sep-2022
 *  FILE NAME  		: 	 DefaultThrowAndOurCatch.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:30:16 pm
 */
package com.RYYoutube.JavaSeries.ExceptionConcept.UncheckedException;
public class DefaultThrowAndOurCatch {
	
	public static void main(String[] args) {
		int a=20; 
		int b=0;
		
		try {//try block will help to excute the statement
			System.out.println("Before exception throw line");
			System.out.println(a/b);
			System.out.println("After exception throw line");
		}catch(ArithmeticException aE){
			System.out.println(aE.getMessage());
			
		}finally {
			System.out.println(a/4);
		}
	}

}
