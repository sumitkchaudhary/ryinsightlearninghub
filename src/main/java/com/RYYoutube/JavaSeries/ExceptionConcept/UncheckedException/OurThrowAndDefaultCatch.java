/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 24-Sep-2022
 *  FILE NAME  		: 	 OurThrowAndDefaultCatch.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    12:53:16 am
 */
package com.RYYoutube.JavaSeries.ExceptionConcept.UncheckedException;
public class OurThrowAndDefaultCatch {
	
	public static void main(String[] args) {
		int balance =5000;
		int withdrawAmt=14000;
		
		if(balance<withdrawAmt) 
			throw new ArithmeticException("You have insufficient Balance");
		balance=balance-withdrawAmt;
		System.out.println("Account balance is"+balance);
		System.out.println("--Continue--");
		
		
	}

}
