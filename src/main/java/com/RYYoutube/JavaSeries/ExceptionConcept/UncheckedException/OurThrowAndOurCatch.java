/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 24-Sep-2022
 *  FILE NAME  		: 	 OurThrowAndOurCatch.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    12:59:46 am
 */
package com.RYYoutube.JavaSeries.ExceptionConcept.UncheckedException;
public class OurThrowAndOurCatch {
	
	public static void main(String[] args) {
		int balance =5000;
		int withdrawAmt=14000;
		
		try {
			if(withdrawAmt>balance) 
				throw new ArithmeticException("You have insufficient Balance");
			balance=balance-withdrawAmt;
			System.out.println("Account balance is"+balance);
		} catch (ArithmeticException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("--Continue--");
		
		

	}

}
