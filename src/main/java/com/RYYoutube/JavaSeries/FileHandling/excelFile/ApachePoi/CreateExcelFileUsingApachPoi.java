package com.RYYoutube.JavaSeries.FileHandling.excelFile.ApachePoi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CreateExcelFileUsingApachPoi {
    public static void main(String[] args) throws IOException {
        XSSFWorkbook workbook=new XSSFWorkbook();
        XSSFSheet sheet =workbook.createSheet("Student Data");
        Object[][] studentData={
                {"RollNumber", "Name", "Last Name"},
                {"01", "Sumit", "Chaudhary"},
                {"02", "Rahul", "Kumar"},
                {"03", "Yogesh", "Chauhan"},
                {"04", "Akash", "Chauhan"},
                {"05", "Jatin", "Kumar"}
        };
        int rowNum=0;
        for(Object[] rowData: studentData){
            Row row=sheet.createRow(rowNum++);
            int colNum=0;
            for(Object field: rowData){
                Cell cell=row.createCell(colNum++);
                if(field instanceof String){
                    cell.setCellValue((String)field);
                }else if(field instanceof  Integer){
                    cell.setCellValue((Integer)field);
                }
            }
        }
        FileOutputStream outputStream=new FileOutputStream("src/main/resources/excelFiles/ApachPoi/StudentData.xlsx");
        workbook.write(outputStream);
        System.out.println("Student data successfully saved in Excel File.");

    }
}
