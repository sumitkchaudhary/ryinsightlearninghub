package com.RYYoutube.JavaSeries.FileHandling.excelFile.ApachePoi;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileInputStream;
import java.io.IOException;
public class ReadExcelDataApachPoi {
    public static void main(String[] args) throws IOException {
        FileInputStream getExcelFile=new FileInputStream("src/main/resources/excelFiles/ApachPoi/StudentData.xlsx");
        Workbook getWorkBook=new XSSFWorkbook(getExcelFile);
        Sheet sheet=getWorkBook.getSheetAt(0);
        for(Row getRow: sheet){
            for(Cell getCellType: getRow){
                switch (getCellType.getCellType()){
                    case STRING:
                        System.out.printf("%-15s",getCellType.getStringCellValue());
                        break;
                    case NUMERIC:
                        System.out.printf("%-15.2f",getCellType.getNumericCellValue());
                        break;
                }
            }
            System.out.println();
        }
    }
}
