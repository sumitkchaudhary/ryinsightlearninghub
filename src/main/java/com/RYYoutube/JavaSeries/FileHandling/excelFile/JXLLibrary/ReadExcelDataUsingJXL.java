/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 23-Jan-2023
 *  FILE NAME  		: 	 ReadExcelDataUsingJXL.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    12:51:49 am
 */
package com.RYYoutube.JavaSeries.FileHandling.excelFile.JXLLibrary;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadExcelDataUsingJXL {
	public static void main(String[] args) throws BiffException, IOException {
		//Fetch the excel file 
		File getFile=new File("../RYYoutubeChannelVideos"
				+ "/src/main/resources/excelFiles/JXLFiles/ExcelWriteData.xls");
		//Fetch the workbook from the file
		Workbook getWorkbook=Workbook.getWorkbook(getFile);
		Sheet getSheet=getWorkbook.getSheet(0);//Fetch the sheet
		for(int row=0; row<getSheet.getRows(); row++) {//Fetch all rows
			for(int column=0; column<getSheet.getColumns(); column++) {//Fetch all column
				Cell getCell=getSheet.getCell(row, column);//Fetch all cell 
				System.out.println(getCell.getContents());//fetch cell data
			}
		}
		
	}

}
