package com.RYYoutube.JavaSeries.FileHandling.excelFile.JXLLibrary;

import com.sun.corba.se.spi.orbutil.threadpool.Work;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.File;
import java.io.IOException;

public class TransferDataSourceToTarget {
    public static void main(String[] args) throws BiffException, IOException, WriteException {
        Workbook sourceBook=Workbook.getWorkbook(new File("src/main/resources/excelFiles/JXLFiles/ExcelWriteData.xls"));
        WritableWorkbook targetWorkBook= Workbook.createWorkbook(new File("src/main/resources/excelFiles/JXLFiles/TargetExcelFile.xls"));

        WritableSheet targetSheet= targetWorkBook.createSheet("Sheet1",0);

        Sheet sourceSheet=sourceBook.getSheet(0);

        for(int i=0; i<sourceSheet.getRows(); i++){
            for(int j=0; j<sourceSheet.getColumns(); j++){
                String cellContent=sourceSheet.getCell(j,i).getContents();
                Label label=new Label(j,i,cellContent);
                targetSheet.addCell(label);
            }
        }

        targetWorkBook.write();
        targetWorkBook.close();
        sourceBook.close();

        System.out.println("Data successfully transfer from source file to target file.");

    }
}
