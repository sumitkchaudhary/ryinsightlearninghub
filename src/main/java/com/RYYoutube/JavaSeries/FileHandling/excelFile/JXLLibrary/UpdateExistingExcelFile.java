package com.RYYoutube.JavaSeries.FileHandling.excelFile.JXLLibrary;


import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.WildcardType;

public class UpdateExistingExcelFile {
    public static void main(String[] args) throws BiffException, IOException, WriteException {
        String excelFilePath="src/main/resources/excelFiles/JXLFiles/UpdateExcelFile.xls";
        Workbook workbook=Workbook.getWorkbook(new File(excelFilePath));
        WritableWorkbook copy=Workbook.createWorkbook(new File(excelFilePath),workbook);
        WritableSheet sheet =copy.getSheet(0);

        int row=sheet.getRows();//Help to get empty row.

        Label rollNumber=new Label(0, row,"5");
        Label name=new Label(1, row,"Prakash");
        Label mobileNumber=new Label(3, row,"9856458596");

        sheet.addCell(rollNumber);
        sheet.addCell(name);
        sheet.addCell(mobileNumber);

        copy.write();
        copy.close();
        workbook.close();

        System.out.println("Your excel file is successfully update please.");


    }
}
