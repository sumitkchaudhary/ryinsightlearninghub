/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 22-Jan-2023
 *  FILE NAME  		: 	 WriteADataIntoAExcelFile.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    1:26:35 am
 */
package com.RYYoutube.JavaSeries.FileHandling.excelFile.JXLLibrary;
import java.io.File;
import java.io.IOException;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.util.Scanner;

public class WriteADataIntoAExcelFile {
	public static void main(String[] args) throws IOException, RowsExceededException, WriteException {
		File filePath=new File("../RYYoutubeChannelVideos/src"
				+ "/main/resources/excelFiles/JXLFiles/ExcelWriteData.xls");
		
		WritableWorkbook createWork=Workbook.createWorkbook(filePath);
		WritableSheet createSheet=createWork.createSheet("DataSheet22123", 0);
		Scanner sc=new Scanner(System.in);
		String userData;
		System.out.println("Please enter your data to store in Excel file.");
		for(int row=0; row<4; row++) {//Create row
			for(int column=0; column<4; column++) {//create column
				userData=sc.nextLine();//Here we take / read data from user/console
				Label setUserDataIntoCell=new Label(column, row, userData); //we set data into cell
				createSheet.addCell(setUserDataIntoCell);//we store a data into sheet
			}
		}
		createWork.write();
		createWork.close();
		
		System.out.println("Your data is successfuly save into a file : "+filePath.getName());
	}

}
