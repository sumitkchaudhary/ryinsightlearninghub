/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 27-Dec-2022
 *  FILE NAME  		: 	 BufferedReadAndWriteTextFile.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    7:57:46 am
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedReadAndWriteTextFile {
	public static void main(String[] args) throws IOException {
		File fileObject=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/bufferedReadWrite.text");
		
		if(!fileObject.exists()) {
			fileObject.createNewFile();
		}else {
			BufferedWriter bw=new BufferedWriter(new FileWriter(fileObject));
			bw.write("Hi guys this the dummy text.");
			bw.write("I'm not new line.");
			bw.newLine();
			bw.write("I'm new line.");
			bw.newLine();
			bw.write("I'm another new line.");
			
			bw.flush();
			bw.close();
			
			BufferedReader br=new BufferedReader(new FileReader(fileObject));
			String fileData=null;
			while((fileData=br.readLine())!=null) {
				System.out.println(fileData);
			}
			br.close();
		}
	}

}
