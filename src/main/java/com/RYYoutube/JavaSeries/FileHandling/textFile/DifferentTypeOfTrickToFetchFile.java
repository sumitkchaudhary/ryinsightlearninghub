/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 16-Jan-2023
 *  FILE NAME  		: 	 DifferentTypeOfTrickToFetchFile.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    7:29:33 am
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class DifferentTypeOfTrickToFetchFile {
	public static void main(String[] args) throws IOException {
		//1. Logic:  Double backslash \\
		//File path=new File("D:\\Sumit_Project_Works\\RYYoutubeChannelVideos\\src\\main\\resources\\textFiles\\bufferedReadWrite.text");
		//2. Logic Single forward slash
		
		/*3. Logic: System.getProperty(""): java.lang.System.getProperty("user.dir")*/
		/*4th Logc: File.separator*/
		
		File path=new File(System.getProperty("user.dir")+File.separator+"src"+File.separator+"main"+File.separator+
				"resources"+File.separator+"textFiles"+File.separator+"bufferedReadWrite.text");
		
		System.out.println("File Path:" +path.getAbsolutePath());
		BufferedReader readFileData=new BufferedReader(new FileReader(path));
		String fileData;
		while((fileData=readFileData.readLine())!=null) {
			System.out.println(fileData);
		}
	}

}
