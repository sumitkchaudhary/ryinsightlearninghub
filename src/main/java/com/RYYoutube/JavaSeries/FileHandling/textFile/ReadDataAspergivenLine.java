/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 29-Dec-2022
 *  FILE NAME  		: 	 ReadDataAspergivenLine.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    9:14:11 pm
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class ReadDataAspergivenLine {
	public static void main(String[] args) throws IOException {
		File fileObject=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/bufferedReadWrite.text");
		LineNumberReader lNR=new LineNumberReader(new FileReader(fileObject));
		
		while(lNR.getLineNumber()!=4) {
			System.out.println(lNR.readLine());		
		}
	}

}
