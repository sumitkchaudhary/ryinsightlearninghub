/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 29-Dec-2022
 *  FILE NAME  		: 	 ReadDataByRange.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    9:29:40 pm
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReadDataByRange {
	public static void main(String[] args) throws IOException {
		File fileObject=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/bufferedReadWrite.text");
		BufferedReader br=new BufferedReader(new FileReader(fileObject));
		
		String fileData;
		int i=0;
		
		while((fileData=br.readLine())!=null) {
			++i;
			//System.out.print(i);
			if(i>=3 && i<=6) {
				System.out.println(fileData);
			}
		}
		
		br.close();
		
	}

}
