/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 29-Dec-2022
 *  FILE NAME  		: 	 ReadSepeceficLineNumberData.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    9:22:29 pm
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadSepeceficLineNumberData {
	public static void main(String[] args) throws IOException {
		File fileObject=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/bufferedReadWrite.text");
		
		String sepeficData=Files.readAllLines(Paths.get(fileObject.toURI())).get(8);
		System.out.println(sepeficData);
	}
}
