/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 29-Dec-2022
 *  FILE NAME  		: 	 SaveUserEnteredData.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:53:22 pm
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SaveUserEnteredData {
	public static void main(String[] args) throws IOException {
		File fileObject=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/userEnteredData.text");
		if(!fileObject.exists()) {
			fileObject.createNewFile();
		}else {
			BufferedWriter bw=new BufferedWriter(new FileWriter(fileObject));
			Scanner sc=new Scanner(System.in);
			System.out.println("How many line you want to enter/ save into the file?");
			int userLine=sc.nextInt();
			System.out.println("Enter your data.");
			String userEnteredData;
			for(int i=0; i<=userLine; i++) {
				userEnteredData=sc.nextLine();
				bw.write(userEnteredData);
				bw.newLine();				
			}
			sc.close();
			bw.flush();
			bw.close();
			System.out.println("Your inputed data is saved into the file please check file or below ."+fileObject.getName());
			
			BufferedReader br=new BufferedReader(new FileReader(fileObject));
			String fileData=null;
			while((fileData=br.readLine())!=null) {
				System.out.println(fileData);
			}
			br.close();
		}
		
	}

}
