/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 10-Dec-2022
 *  FILE NAME  		: 	 TextFileWriteAndReadData.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:12:08 pm
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileWriteAndReadData {
	public static void main(String[] args) throws IOException {
		
		//D:\Sumit_Project_Works\RYYoutubeChannelVideos\src\main\resources\textFiles
		File fileObject=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/ReadWriteTextFile.text");
		
		if(!fileObject.exists()) {
			fileObject.createNewFile();
		}else {
			FileWriter fileWriterObject=new FileWriter(fileObject);
			fileWriterObject.write("Hi Everyone this is our dummy data for understanding.");
			fileWriterObject.flush();
			fileWriterObject.close();
			
			FileReader fileReaderObject=new FileReader(fileObject);
			int readData;
			while((readData=fileReaderObject.read())!=-1) {
				System.out.print((char)readData);
			}
			fileReaderObject.close();
		}
	}

}
