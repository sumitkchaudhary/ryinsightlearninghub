/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 01-Jan-2023
 *  FILE NAME  		: 	 TransferDataFromSourceToDestinationFile.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    9:02:45 pm
 */
package com.RYYoutube.JavaSeries.FileHandling.textFile;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TransferDataFromSourceToDestinationFile {
	public static void main(String[] args) throws IOException {
		File sourceFile=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/bufferedReadWrite.text");
		
		BufferedReader br=new BufferedReader(new FileReader(sourceFile));
		String fileData;
		File destinationFile=new File("../RYYoutubeChannelVideos/src/main/resources/textFiles/destinationFile.text");
		BufferedWriter bw=new BufferedWriter(new FileWriter(destinationFile));
		bw.write("This is desitnation file where we save "+sourceFile.getName()+" data");
		bw.newLine();
		while((fileData=br.readLine())!=null) {
			bw.write(fileData);
			bw.newLine();
		}
		bw.close();
		br.close();
		
	}

}
