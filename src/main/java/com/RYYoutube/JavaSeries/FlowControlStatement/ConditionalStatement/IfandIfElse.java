/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 IfandIfElse.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:01:01 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.ConditionalStatement;
public class IfandIfElse {
	
	public static void main(String[] args) {
		String s1="Sumit";
		String s2="Sushil";
		if(s1.equals(s2)) {
			System.out.println(true);
		}else if(s1.equals("Amit")) {
			System.out.println(true);
		}
		
		else {
			System.out.println(false);
		}
	}

}
