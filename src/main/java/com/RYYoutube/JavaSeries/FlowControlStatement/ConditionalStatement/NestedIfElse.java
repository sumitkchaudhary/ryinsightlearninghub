/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 NestedIfElse.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:03:57 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.ConditionalStatement;
public class NestedIfElse {
	
	public static void main(String[] args) {
		int i=11;
		if(i>0) {
			if(i==10) {
				System.out.println(true);
			}else {
				System.out.println(false);
			}
		}else {
			System.out.println(i+" is negative");
		}
	}

}
