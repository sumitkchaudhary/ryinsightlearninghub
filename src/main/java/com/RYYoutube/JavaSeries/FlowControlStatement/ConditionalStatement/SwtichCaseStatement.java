/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 SwtichCaseStatement.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:07:40 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.ConditionalStatement;

import java.util.Scanner;

public class SwtichCaseStatement {
	public static void main(String[] args) {
		int a=0; int b=0; int c=0;
		System.out.println("Please enter two number for perform the calculation");
		Scanner sc=new Scanner(System.in);
		a=sc.nextInt();
		b=sc.nextInt();
		
		System.out.println("Please enter desire operator to perform calculation\n "
				+"Example \r\n1. +\r\n 2. -\r\n 3. *\r\n 4. /\r\n 5. %");
		
		char operator=sc.next().charAt(0);
		System.out.println("Your entered operator is :"+operator);
		
		switch(operator) {
		case '+': c=a+b;
		break;
		case '-': c=a-b;
		break;
		case '*': c=a*b;
		break;
		case '/': c=a/b;
		break;
		case '%': c=a%b;
		break;
		default: 
			System.out.println("You have entered wrong operator"+operator);
		}
		
		System.out.println("Your calculation value is: "+c);
		
	}

}
