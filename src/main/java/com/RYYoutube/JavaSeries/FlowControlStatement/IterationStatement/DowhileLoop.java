/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 DowhileLoop.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:38:51 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.IterationStatement;
public class DowhileLoop {
	public static void main(String[] args) {
		int no=1;
		do {
			System.out.println(no);
			no=no+1;
		}while(no<=100);
	}

}
