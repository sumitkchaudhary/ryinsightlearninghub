/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 ForEachLoop.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:26:05 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.IterationStatement;
public class ForEachLoop {
	
	public static void main(String[] args) {
		String fruitList[]= {"Apple","Orange","Banana","Papaya","Mango"};
		
		double mobileNumberList[]= {34324.50,42343.75,4354.500,67.67,45.45};
		
		for(double mbNo: mobileNumberList) {
			System.out.println(mbNo);
		}
	}

}
