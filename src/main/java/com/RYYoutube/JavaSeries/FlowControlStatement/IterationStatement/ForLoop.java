/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 ForLoop.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:20:44 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.IterationStatement;
public class ForLoop {
	public static void main(String[] args) {
		int number=345;
		
		for(int i=1; i<=10; i++) {
			System.out.println(number+"X"+i+"="+number*i);
		}
	}

}
