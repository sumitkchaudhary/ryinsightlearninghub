/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 WhileLoop.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:34:32 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.IterationStatement;
public class WhileLoop {
	public static void main(String[] args) {
		
		int no=1;
		while(no<=100) {
			System.out.println(no);
			no=no+1;
		}
	}

}
