/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 BreakStatement.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:40:37 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.JumpStatement;
public class BreakStatement {
	
	public static void main(String[] args) {
		for(int i=1; i<=100; i++) {
			System.out.println(i);
			if(i==50) {
				break;
			}
		}
	}

}
