/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Aug-2022
 *  FILE NAME  		: 	 Continue.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    4:42:04 pm
 */
package com.RYYoutube.JavaSeries.FlowControlStatement.JumpStatement;
public class Continue {
	public static void main(String[] args) {
		for(int i=1; i<=10; i++) {
			
			if(i > 4 && i<9) {
				continue;
			}
			System.out.println(i);
		}
	}


}
