package com.RYYoutube.JavaSeries.Inheritance.Hierachical;

public class HierarchicalClassB extends HierarchicalClassA {
	
	public void HierarchicalClassBMethod() {
		System.out.println("I'm Hierarchical classB method");
	}
	
	public static void main(String[] args) {
		HierarchicalClassB objB=new HierarchicalClassB();
		objB.HierarchicalClassAMethod();
		objB.HierarchicalClassBMethod();
	}
}
