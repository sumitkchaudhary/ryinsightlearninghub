package com.RYYoutube.JavaSeries.Inheritance.Hierachical;

public class HierarchicalClassC  extends HierarchicalClassA{
	
	public void HierarchicalClassCMethod() {
		System.out.println("I'm Hierarchical Class C Method");
	}
	
	public static void main(String[] args) {
		HierarchicalClassC objC=new HierarchicalClassC();
		objC.HierarchicalClassAMethod();
		objC.HierarchicalClassCMethod();
	}

}
