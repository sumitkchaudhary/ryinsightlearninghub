package com.RYYoutube.JavaSeries.Inheritance.Multilevel;

public class MultiClassA {
	
	public void multiClassAMethod() {
		System.out.println("I'm multiClassA Method");
	}

	public static void main(String[] args) {
		MultiClassA obja=new MultiClassA();
		obja.multiClassAMethod();
	}
}
