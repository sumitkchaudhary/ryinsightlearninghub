package com.RYYoutube.JavaSeries.Inheritance.Multilevel;

public class MultiClassB extends MultiClassA {
	
	public void multiClassBMethod() {
		System.out.println("I'm multiClassB Method");
	}
	
	public static void main(String[] args) {
		MultiClassB objb=new MultiClassB();
		objb.multiClassAMethod();
		objb.multiClassBMethod();
		
		
	}

}
