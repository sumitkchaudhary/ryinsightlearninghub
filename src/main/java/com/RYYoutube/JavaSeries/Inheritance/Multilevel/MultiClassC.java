package com.RYYoutube.JavaSeries.Inheritance.Multilevel;

public class MultiClassC extends MultiClassB{
	
	public void multiClassCMethod() {
		System.out.println("I'm multiClassC Method");
	}
	
	public static void main(String[] args) {
		MultiClassC objc=new MultiClassC();
		objc.multiClassAMethod();
		objc.multiClassBMethod();
		objc.multiClassCMethod();
		
	}

}
