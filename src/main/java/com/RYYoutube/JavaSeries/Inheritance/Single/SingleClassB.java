package com.RYYoutube.JavaSeries.Inheritance.Single;

public class SingleClassB extends SingleClassA {
	
	public void classBMethod() {
		System.out.println("I'm a method of SingleClass B");
	}
	
	public static void main(String[] args) {
		SingleClassA objA=new SingleClassA();
		objA.classAMethod();
		
		//objA.classBMethod();
		
		SingleClassB obj=new SingleClassB();
		obj.classAMethod();
		obj.classBMethod();
	}

}
