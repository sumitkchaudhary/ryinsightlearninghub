package com.RYYoutube.JavaSeries.Keywords;

public class SuperKeyword extends ThisKeywords{
	
	public void superKeywordOneParametriseMethod(int a) {
		super.thisKeywordsFourParametriseMethod(80, 10, 30, 50);
		System.out.println("I'm a one parametrise method from super keyword class. "+a+" "+super.memberVariable);
	}
	public void superKeywordTwoParametriseMethod(int a, int b) {
		this.superKeywordOneParametriseMethod(10);
		System.out.println("I'm a two parametrise method from super keyword class. "+a+b);
	}
	public void superKeywordThreeParametriseMethod(int a, int b, int c) {
		this.superKeywordTwoParametriseMethod(50, 70);
		System.out.println("I'm a three parametrise method from super keyword class."+a+b+c);
	}
	public void superKeywordFourParametriseMethod(int a, int b, int c, int d) {
		this.superKeywordThreeParametriseMethod(20, 40, 50);
		System.out.println("I'm a four parametrise method from super keyword class."+a+b+c+d);
	}
	
	public static void main(String[] args) {
		SuperKeyword obje=new SuperKeyword();
		obje.superKeywordFourParametriseMethod(70, 10, 50, 70);
	}

}
