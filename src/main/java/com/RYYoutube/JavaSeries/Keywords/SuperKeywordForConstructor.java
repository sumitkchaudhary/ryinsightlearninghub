package com.RYYoutube.JavaSeries.Keywords;

public class SuperKeywordForConstructor extends ThisKeywordsforConstructor{
	
	public SuperKeywordForConstructor(int a) {
		super(50, 60, 40, 60);
		System.out.println("I'm a one parametrise constructor from SuperKeywordForConstructor. "+a+" "+memberVariable);
	}
	public SuperKeywordForConstructor(int a, int b) {
		this(10);
		System.out.println("I'm a two parametrise constructor from SuperKeywordForConstructor "+a+b);
	}
	public SuperKeywordForConstructor(int a, int b, int c) {
		this(50, 70);
		System.out.println("I'm a three parametrise constructor from SuperKeywordForConstructor"+a+b+c);
	}
	public SuperKeywordForConstructor(int a, int b, int c, int d) {
		this(20, 40, 50);
		System.out.println("I'm a Four parametrise constructor from SuperKeywordForConstructor"+a+b+c+d);
	}
	
	
	public static void main(String[] args) {
		SuperKeywordForConstructor obje=new SuperKeywordForConstructor(10,20,30,40);
	}

}
