package com.RYYoutube.JavaSeries.Keywords;

public class ThisKeywords {
	String memberVariable;
	
	public void thisKeywordsOneParametriseMethod(int a) {
		this.memberVariable="I'm member variable of this keyword class";
		System.out.println("I'm a one parametrise method from this keyword class. "+a+" "+memberVariable);
	}
	public void thisKeywordsTwoParametriseMethod(int a, int b) {
		this.thisKeywordsOneParametriseMethod(10);
		System.out.println("I'm a two parametrise method from this keyword class. "+a+b);
	}
	public void thisKeywordsThreeParametriseMethod(int a, int b, int c) {
		this.thisKeywordsTwoParametriseMethod(50, 70);
		System.out.println("I'm a three parametrise method from this keyword class."+a+b+c);
	}
	public void thisKeywordsFourParametriseMethod(int a, int b, int c, int d) {
		this.thisKeywordsThreeParametriseMethod(20, 40, 50);
		System.out.println("I'm a Four parametrise method from this keyword class."+a+b+c+d);
	}
	

}
