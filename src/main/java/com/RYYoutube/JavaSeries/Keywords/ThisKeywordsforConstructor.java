package com.RYYoutube.JavaSeries.Keywords;

public class ThisKeywordsforConstructor {
	String memberVariable;
	
	public ThisKeywordsforConstructor(int a) {
		this.memberVariable="I'm member variable of ThisKeywordsforConstructor class";
		System.out.println("I'm a one parametrise constructor from ThisKeywordsforConstructor. "+a+" "+memberVariable);
	}
	public ThisKeywordsforConstructor(int a, int b) {
		this(10);
		System.out.println("I'm a two parametrise constructor from ThisKeywordsforConstructor "+a+b);
	}
	public ThisKeywordsforConstructor(int a, int b, int c) {
		this(50, 70);
		System.out.println("I'm a three parametrise constructor from ThisKeywordsforConstructor"+a+b+c);
	}
	public ThisKeywordsforConstructor(int a, int b, int c, int d) {
		this(20, 40, 50);
		System.out.println("I'm a Four parametrise constructor from ThisKeywordsforConstructor"+a+b+c+d);
	}
	
	public static void main(String[] args) {
		ThisKeywordsforConstructor obj=new ThisKeywordsforConstructor(40,60,50,30);
	}

}
