package com.RYYoutube.JavaSeries.MemoryManagement;

class StudentData{
    int rollNumber;
    String StudentName;
    public StudentData(int rollNumber, String StudentName){
        this.rollNumber=rollNumber;
        this.StudentName=StudentName;
    }
}
public class MemoryExample {
    public static StudentData studentInfo(int rl, String sn){
        return new StudentData(rl, sn);
    }

    public static void main(String[] args) {
        int rollNumber=124;
        String name="Sumit";
        StudentData studentDataReferenceVariable=null;
        studentDataReferenceVariable=studentInfo(rollNumber,name);
    }
}
