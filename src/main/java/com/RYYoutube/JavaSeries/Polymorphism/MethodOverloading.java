package com.RYYoutube.JavaSeries.Polymorphism;

public class MethodOverloading {
	
	public void method() {
		System.out.println("Without parameter");
	}
	
	public void method(int a) {
		System.out.println(a);
	}
	
	public void method(String a) {
		System.out.println(a);
	}
	
	public void method(int  a, int b) {
		System.out.println(a+b);
	}
	public void method(int  a, double b) {
		System.out.println(a+b);
	}
	
	public static void main(String[] args) {
		MethodOverloading referenceVariable=new MethodOverloading();
		referenceVariable.method(60,60.60);
	}

}
