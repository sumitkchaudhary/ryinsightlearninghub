package com.RYYoutube.JavaSeries.Polymorphism;
public class MethodOverriding extends MethodOverloading {
	@Override
	public void method() {
		System.out.println("Child Without parameter");
	}
	@Override
	public void method(int a) {
		System.out.println(a+"Message");
	}
	@Override
	public void method(String a) {
		System.out.println(a+10);
	}
	@Override
	public void method(int  a, int b) {
		System.out.println(a-b);
	}
	@Override
	public void method(int  a, double b) {
		System.out.println(a-b);
	}
	public void method(int  a, float b) {
		System.out.println(a-b);
	}
	public static void main(String[] args) {
		MethodOverriding referenceChildVariable=new MethodOverriding();
		
		referenceChildVariable.method();
		referenceChildVariable.method(10);
		referenceChildVariable.method("Child Message");
		referenceChildVariable.method(70,30);
		referenceChildVariable.method(100,50.50);
		referenceChildVariable.method(100,50f);
	}

}
