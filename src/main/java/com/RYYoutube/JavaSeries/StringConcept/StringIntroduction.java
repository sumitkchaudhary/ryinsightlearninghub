/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 07-Aug-2022
 *  FILE NAME  		: 	 StringIntroduction.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    6:35:41 pm
 */
package com.RYYoutube.JavaSeries.StringConcept;
public class StringIntroduction {
	public static void main(String[] args) {
		/*String is a non-primitive data type */
		String s1="Sumit";
		
		
		s1.concat("Chaudhary");
		System.out.println(s1);
		
		s1=s1.concat(" Kumar Chaudhary");
		System.out.println(s1);
		String s2=s1.concat(" Chaudhary");
		System.out.println(s2);
		
	}

}
