/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 30-Jul-2022
 *  FILE NAME  		: 	 PersonInformation.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    1:05:04 pm
 */
package com.RYYoutube.JavaSeries.abstractConcept;
public abstract class PersonInformation {
	
	String message; 
	
	
	//Abstract method and it have no body
	abstract void studentRollNumber();
	
	abstract void studentCourse();
	
	abstract void facultyEmpId();
	
	abstract void facultySubject();
	
	abstract String personaFullName(String name);
	
	//Non-abstract method / normal method
	public void classUserMessage() {//From here method body is start
		
	}//from here method body is end
	
	//Non-abstract method / normal method
	public int id(int i) {//From here method body is start
		return i;
	}//from here method body is end

}
