/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 30-Jul-2022
 *  FILE NAME  		: 	 SchoolData.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    1:12:05 pm
 */
package com.RYYoutube.JavaSeries.abstractConcept;
public class SchoolData extends PersonInformation{

	@Override
	void studentRollNumber() {
		System.out.println("St_001");	
	}

	@Override
	void studentCourse() {
		System.out.println("Java Language");
		
	}

	@Override
	void facultyEmpId() {
		System.out.println("F_001");
		
	}

	@Override
	void facultySubject() {
		System.out.println("Java");
		
	}

	@Override
	String personaFullName(String name) {
		
		return name;
	}
	
	

}
