/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 30-Jul-2022
 *  FILE NAME  		: 	 SchoolDataFetch.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    1:17:22 pm
 */
package com.RYYoutube.JavaSeries.abstractConcept;
public class SchoolDataFetch {
	
	public static void main(String[] args) {
	
		String s="new";
		SchoolData schoolD=new SchoolData();
		
		schoolD.personaFullName("Student_ Sumit Chaudhary");
		schoolD.studentRollNumber();
		schoolD.studentCourse();
		
		schoolD.personaFullName("Faculty Name_ Sushil Chaudhary");
		schoolD.facultyEmpId();
		schoolD.facultySubject();
		
		System.out.println(schoolD.message="All data fetched");
		
		
	}

}
