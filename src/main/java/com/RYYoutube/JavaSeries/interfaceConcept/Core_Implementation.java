/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 30-Jul-2022
 *  FILE NAME  		: 	 Core_Implementation.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    1:29:29 pm
 */
package com.RYYoutube.JavaSeries.interfaceConcept;
public interface Core_Implementation {
	
	abstract void addition();
	
	abstract void addition(int a, int b);
	
	//We can create normal method without body
	public void message();
	
	
	//public static int addition();
	//In interface class we can't create static/ return tupe method
	
	//We can't not create normal method with body
	/*public void messageInro(){
		
	}*/
	
	/* We can achieve 100% abstraction with the help interface 
	 * we can hide the core implementation/ declaration of subclass. But subclass must be 
	 * implements that methods which is define in the interface class
	 * We can achieve multiple inheritance
	 * We can't create object of the interface class
	 * 
	 * 
	 * */
}
