/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 30-Jul-2022
 *  FILE NAME  		: 	 Core_ImplementationMessages.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    1:38:09 pm
 */
package com.RYYoutube.JavaSeries.interfaceConcept;
public interface Core_ImplementationMessages extends Core_Implementation{
	
	abstract void coreImMessageFirst();
}
