/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 30-Jul-2022
 *  FILE NAME  		: 	 ImplementingInterface.java
 *  PROJECT NAME 	:	 JAVAConceptExploring
 * 	Class Time		:    1:39:54 pm
 */
package com.RYYoutube.JavaSeries.interfaceConcept;
public class ImplementingInterface implements Core_Implementation, Core_ImplementationMessages{

	@Override
	public void coreImMessageFirst() {
		System.out.println("Implemeted method which declared in Core_ImplementationMessages");
		
	}

	@Override
	public void addition() {
		System.out.println("Addition");
		
	}

	@Override
	public void addition(int a, int b) {
		System.out.println(a+b);
		
	}

	@Override
	public void message() {
		System.out.println("Normal method implementation");
	}
	
	public static void main(String[] args) {
		ImplementingInterface referenceVariable=new ImplementingInterface();
				
		referenceVariable.addition();
		referenceVariable.addition(10,10);
		referenceVariable.coreImMessageFirst();
		referenceVariable.message();
		
	}

}
