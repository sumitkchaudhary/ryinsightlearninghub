package com.RYYoutube.JavaSeries.introductionConcept;
import java.awt.Container;

public class ConstructorsConcept {
	
	int intValue;
	String msg;
	public ConstructorsConcept() {
		System.out.println("I'm default constructor");
	}
	
	public ConstructorsConcept( int a) {
		System.out.println("I'm one parameter constructor"+a);
	}
	
	public ConstructorsConcept(int a, char c) {
		System.out.println("I'm two  parameterize constructor"+a+(c));
	}
	public ConstructorsConcept(int a, int b, int c) {
		System.out.println("I'm three parametrise constructor"+(a+" "+b+" "+c));
	}
	public ConstructorsConcept(int intValue, String msg) {
		//This keyword help to refer the variable which define with in class
		this.intValue=intValue;
		this.msg=msg;
		System.out.println("I'm two parametris with global variable constructor  "+intValue+"  "+msg);
	}
	
	public void messageShow() {
		System.out.println(intValue + msg);
	}
	
	public static void main(String[] args) {
		ConstructorsConcept obj=new ConstructorsConcept(10, "Hello everyone how are you");
		
		obj.messageShow();
	}

}
