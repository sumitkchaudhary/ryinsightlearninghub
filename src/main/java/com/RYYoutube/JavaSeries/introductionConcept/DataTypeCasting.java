package com.RYYoutube.JavaSeries.introductionConcept;
public class DataTypeCasting {
	
	public static void main(String[] args) {
		/* We can achieve data type casting in two ways
		 * - implicit type casting/ Automatic conversion / Widening 
		 * - explicit type casting/ manually conversion / Narrowing type casting
		 * */
		
		//Implicit 
			int intVariable=10;
			
			float floatVaraible=intVariable;
			
			System.out.println(floatVaraible);
			
		//Explicit 
			float floatVar=10.50f;
			
			int intVar=(int)floatVar;
	}

}
