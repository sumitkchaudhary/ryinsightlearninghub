package com.RYYoutube.JavaSeries.introductionConcept;
public class DataTypes {
	
	public static void main(String[] args) {
		/*Data Types 
		 * 1. Primitive Data Type : Predefine by jaca 
		 * 		
		 * */
		
		boolean b; //True , False  Default Value - False, Size 1 bit
		char c;  // /u000  Size 16bits   character representation of ASCII values 0 to 255
		int i;    //0, Size 32 bits    -2147483648 to 2147483647
		short s;  //0, Size 16 bits  -32768 to 32767
		byte by; //0, Size 8 bits  -128 to 127 
		long l;  //0, Size 64 bits  -9223372036854775808 to 9223372036854775807
		float f;  //0.0  32 bits    upto 7 decimal digits 
		double d; //0.0  65 bits    upto 16 decimal digits 
		
		//2 Non-Primitive Data type -   -- Define by user as per requirement , Class , Interface, and Arrays
		
	}

}
