package com.RYYoutube.JavaSeries.introductionConcept;
public class MethodsTypes {
	
	//Non-Static but return type method
	public int sum(int a, int b) {
		return a+b;
	}
	
	//Static but no return type
	public static void showMessage(String msg) {
		System.out.println(msg);
	}
	//non return type method
	public void nonReturnTypeMethod() {
		System.out.println("I'm non return type method. For call me you need to create an object of the class");
	}
	
	public static String myMessage(String msg) {
		return msg;
	}
	
	public static void main(String[] args) {
		MethodsTypes.showMessage("I'm return type method ");
		
		System.out.println(MethodsTypes.myMessage("I'm static method with return type "));
		MethodsTypes obj=new MethodsTypes();
		
		
		obj.sum(50, 40);
		obj.nonReturnTypeMethod();
	}

}
