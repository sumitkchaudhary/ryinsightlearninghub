package com.RYYoutube.JavaSeries.introductionConcept;
public class Operators {
	
	public static void main(String[] args) {
		
		/* Assignment Operator : =
		 * Arithmetic operator : +, -, /, %, ++, --
		 * Relational Operator : >, <, >=, <=, ++, !=
		 * Logical Operator :  &&, ||, &|, !^
		 * Conditonal : ?
		 * 
		 * */
		int i=100;		int j=300 ;int k=500;
		//Logical
		/*
		System.out.println(i>j);
		System.out.println(j>k);
		
		System.out.println(i<j);
		System.out.println(j<k);
		
		System.out.println(i<=j);
		System.out.println(j<=k);
		
		
		System.out.println(i>=j);
		System.out.println(j>=k);
		
		
		System.out.println(i==j);
		System.out.println(j==k);
		
		System.out.println(i!=j);
		System.out.println(j!=k);
		*/
		//Logical Operator 
		boolean value1=true;
		boolean value2=false;
		/*
		System.out.println("Are both value true : "+(value1 && value2));
		
		System.out.println("From both any other value  is true : "+(value1 || value2));
		
		System.out.println("state of value1 to false : "+(!value1 ));
		*/
		
		//Conditional Operator
		
		int ten=10; int twenty=20;  int thirty=30;  boolean bValue;  int iValue;
		
		bValue=(thirty==twenty+ten) ? true: false; 
		System.out.println(bValue);  //true
		
		iValue=(thirty==twenty+ten) ? 50: 100; 
		System.out.println(iValue);  // 50
		
		iValue=(!(thirty==twenty + ten))? 50: 100;
		System.out.println(iValue); //100
		 
		
		
	}

}
