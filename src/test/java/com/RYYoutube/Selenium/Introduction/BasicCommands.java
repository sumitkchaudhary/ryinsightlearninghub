/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2023
 *  FILE NAME  		: 	 BasicCommands.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    12:24:13 am
 */
package com.RYYoutube.Selenium.Introduction;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class BasicCommands {
	/* 1. Launch a browser 
	 * 2. Open text box screen
	 * 3. Click check menu
	 * 4. Get the text of the menu and validate 
	 * 5. Click a back button / go to the back/ previouse screen
	 * 6. Get the text and validate
	 * 
	 * */

	public static void main(String[] args) {
		System.setProperty("webdriver.edge.driver", "D:\\BrowserDriver\\edgedriver_win64\\msedgedriver.exe");
		WebDriver driver= new EdgeDriver();
		driver.manage().window().maximize();
				
		driver.navigate().to("https://demoqa.com/text-box");
		driver.findElement(By.xpath("//span[text()='Check Box']")).click();
		//Frist comman
		String cbPTitle=driver.findElement(By.xpath("//div[@class='main-header']")).getText();
		if(cbPTitle.contains("Check Box")) {
			System.out.println("Test Case Pass");
		}else {
			System.out.println("Test case failed");
		}
		//Second command
		driver.navigate().back();
		String tbPTitle=driver.findElement(By.xpath("//div[@class='main-header']")).getText();
		if(tbPTitle.contains("Text Box")) {
			System.out.println("Test Case Pass");
		}else {
			System.out.println("Test case failed");
		}
		
		//Refresh the page
		driver.navigate().refresh();
		//Go to the forward screen
		driver.navigate().forward();
		//Get page title
		System.out.println(driver.getTitle());
		//Get page source
		System.out.println(driver.getPageSource());
		
		
	}

}
