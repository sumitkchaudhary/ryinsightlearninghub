/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 05-Mar-2023
 *  FILE NAME  		: 	 SelectorHub.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    1:22:56 pm
 */
package com.RYYoutube.Selenium.Introduction;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class SelectorHub {
	public static void main(String[] args) {
		System.setProperty("webdriver.edge.driver", "D:\\BrowserDriver\\edgedriver_win64\\msedgedriver.exe");
		WebDriver driver= new EdgeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://selectorshub.com/xpath-practice-page/");
		driver.findElement(By.xpath("//input[@id='userId']")).sendKeys("wackysumit@gmail.com");
		driver.findElement(By.cssSelector("#pass")).sendKeys("123455");
		driver.findElement(By.name("company")).sendKeys("R&Y Insight Learning Hub");
		driver.findElement(By.xpath("/html[1]/body[1]/div[1]/main[1]/div[1]/div[1]/section[2]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[2]")).sendKeys("1234");
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
	}

}
