/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 04-Feb-2023
 *  FILE NAME  		: 	 Setup.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    7:32:05 pm
 */
package com.RYYoutube.Selenium.Introduction;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Setup {
	public static void main(String[] args) {
		System.setProperty("webdriver.edge.driver", 
				"D:\\BrowserDriver\\edgedriver_win64\\msedgedriver.exe");
		WebDriver driver= new EdgeDriver();
		driver.navigate().to("https://www.google.com/");
		driver.manage().window().maximize();
		//driver.close();
	}

}
