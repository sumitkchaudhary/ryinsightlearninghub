/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 01-Apr-2023
 *  FILE NAME  		: 	 WebDriverManagerIntroduction.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    11:53:15 pm
 */
package com.RYYoutube.Selenium.Introduction;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebDriverManagerIntroduction {
	public static void main(String[] args) {
		WebDriverManager.firefoxdriver().setup();
		WebDriver driver=new FirefoxDriver();
		/*driver.navigate().to("https://demoqa.com/forms");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
		driver.findElement(By.xpath("//span[normalize-space()='Practice Form']")).click();
		driver.navigate().to("https://demoqa.com/forms");
		WebDriverWait waitActivity=new WebDriverWait(driver, Duration.ofSeconds(20));
		WebElement formMenu=driver.findElement(By.xpath("//span[normalize-space()='Practice Form']"));
		waitActivity.until(ExpectedConditions.elementToBeClickable(formMenu)).click();*/
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
		WebDriverWait waitActivity=new WebDriverWait(driver, Duration.ofSeconds(100));
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@name='username']")))).sendKeys("Admin");
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@name='password']")))).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		waitActivity.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.partialLinkText("viewBuzz")))).click();
		WebElement postBox=driver.findElement(By.xpath("//textarea[@placeholder=\"What's on your mind?\"]"));
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		//Specify the timout of the wait
		wait.withTimeout(Duration.ofSeconds(60));
		//Sepcify polling time
		wait.pollingEvery(Duration.ofSeconds(60));
		//Specify what exceptions to ignore
		wait.ignoring(NoSuchElementException.class);

		//This is how we specify the condition to wait on.
		//This is what we will explore more in this chapter
		wait.until(ExpectedConditions.visibilityOf(postBox));
		postBox.sendKeys("I'm Automation tester and this is scripted comments.");
		waitActivity.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//button[@type='submit']")))).click();

		 
	}
	/*
	public static void flauntWaitMethod() {
		/*Fluent Wait in Selenium
			Fluent Wait in Selenium marks the maximum amount of time for Selenium WebDriver to wait for a certain condition (web element) becomes visible. 
			It also defines how frequently WebDriver will check if the condition appears before throwing the “ElementNotVisibleException”.
			To put it simply, Fluent Wait looks for a web element repeatedly at regular intervals until timeout happens or until the object is found.
			Fluent Wait commands are most useful when interacting with web elements that can take longer durations to load. This is something that often occurs in Ajax applications.
			While using Fluent Wait, it is possible to set a default polling period as needed. The user can configure the wait to ignore any exceptions during the polling period.
			Fluent waits are also sometimes called smart waits because they don’t wait out the entire duration defined in the code. Instead, the test continues to execute as soon as
			 the element is detected – as soon as the condition specified in .until(YourCondition) method becomes true.
		 * */
		//Declare and initialise a fluent wait
		/*
		launchBrowser("firfox");
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@id='txtUsername']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@id='txtPassword']")).sendKeys("admin123");
		driver.findElement(By.xpath("//input[@id='btnLogin']")).click();
		driver.findElement(By.xpath("//b[normalize-space()='Buzz']")).click();
		WebElement postBox=driver.findElement(By.xpath("//textarea[@id='createPost_content']"));
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		//Specify the timout of the wait
		wait.withTimeout(Duration.ofSeconds(60));
		//Sepcify polling time
		wait.pollingEvery(Duration.ofSeconds(60));
		//Specify what exceptions to ignore
		wait.ignoring(NoSuchElementException.class);

		//This is how we specify the condition to wait on.
		//This is what we will explore more in this chapter
		wait.until(ExpectedConditions.visibilityOf(postBox));
		postBox.sendKeys("I'm Automation tester and this is scripted comments.");
		driver.findElement(By.xpath("//input[@id='postSubmitBtn']")).click();
		*/
	//}


}
