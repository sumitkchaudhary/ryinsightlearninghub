/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 28-Feb-2023
 *  FILE NAME  		: 	 Basic_CommonLocator.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    12:26:00 am
 */
package com.RYYoutube.Selenium.locatorstratagies;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;

public class Basic_CommonLocator {
	public static void main(String[] args) {
		System.setProperty("webdriver.edge.driver", "D:\\BrowserDriver\\edgedriver_win64\\msedgedriver.exe");
		WebDriver driver= new EdgeDriver();
		driver.navigate().to("https://www.google.com/");
		driver.manage().window().maximize();
		/*//1. ID
		driver.findElement(By.id("email")).sendKeys("wackysumit@gmail.com");
		
		//2. name
		driver.findElement(By.name("pass")).sendKeys("randompass");
		
		//3. tagName
		driver.findElement(By.tagName("button")).click();
		
		// 4. Linktext
		driver.findElement(By.linkText("Forgotten password?")).click();*/
		
		//5. Partiallinktext
		//driver.findElement(By.partialLinkText("password")).click();
		
		//6. className
		WebElement googleSearchBar=driver.findElement(By.className("gLFyf"));
		
		googleSearchBar.sendKeys("R&Y Insight Learning Hub");
		googleSearchBar.sendKeys(Keys.ENTER);
		
	}

}
