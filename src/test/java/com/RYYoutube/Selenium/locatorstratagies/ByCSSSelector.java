/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 28-Feb-2023
 *  FILE NAME  		: 	 ByCSSSelector.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:00:17 am
 */
package com.RYYoutube.Selenium.locatorstratagies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;

public class ByCSSSelector {
	public static void main(String[] args) {
		System.setProperty("webdriver.edge.driver", "D:\\BrowserDriver\\edgedriver_win64\\msedgedriver.exe");
		WebDriver driver= new EdgeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://www.facebook.com/");
		//1.CSS Selector rule - #id
		driver.findElement(By.cssSelector("#email")).sendKeys("sumitk440@gmail.com");
		
		//2. CSS Selector rule - tag#id
		driver.findElement(By.cssSelector("input#pass")).sendKeys("randomepass");
		
		//3. CSS selector rule -- tagName
		driver.findElement(By.cssSelector("[name='login']")).click();
		
		
		//4.CSS Selector rule - tag[attributeName='attributeValue']
		WebElement uName=driver.findElement(By.cssSelector("input[placeholder='Email address or phone number']"));
		//uName.click();
		uName.clear();
		uName.sendKeys("randmeemail@email.com");
	//	uName.sendKeys(Keys.arr)
		
		//5. CSS Selector rule - start with : [attribute^=attriValue]
		WebElement pass=driver.findElement(By.cssSelector("[type^=pass]"));
		pass.click();
		pass.sendKeys("randomepass");
				
		//6. CSS selector rule -- end with [attribute$=attriValue]
		driver.findElement(By.cssSelector("[id$='button']")).click();
				
		
		
	}

}
