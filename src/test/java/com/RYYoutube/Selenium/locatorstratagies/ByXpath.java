/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 01-Mar-2023
 *  FILE NAME  		: 	 ByXpath.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    11:30:43 pm
 */
package com.RYYoutube.Selenium.locatorstratagies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class ByXpath {
	public static void main(String[] args) {
		System.setProperty("webdriver.edge.driver", "D:\\BrowserDriver\\edgedriver_win64\\msedgedriver.exe");
		WebDriver driver= new EdgeDriver();
		driver.manage().window().maximize();
		
		driver.navigate().to("https://demoqa.com/automation-practice-form");
		//Relative xpath 
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/input")).sendKeys("Sumit");
		
		//Absolute xpath -- 1. Static/ basic xpath //tagName[@attribute='attributeValue']
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Kumar");
		
		//Absolute xpath - 2. Dynamic // advance xpath -- contains
		driver.findElement(By.xpath("//input[contains(@id,'Email')]")).sendKeys("wackysumit@gmail.com");
		
		//Absolute xpath - 3. starts-with 
		driver.findElement(By.xpath("//*[text()='Male']")).click();
				
		//Absolute xpath - 4. starts-with 
		driver.findElement(By.xpath("//*[starts-with(@placeholder,'Mobile')]")).sendKeys("123456778");
		
	}

}
