/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 03-Mar-2023
 *  FILE NAME  		: 	 ByXpathAndOROperator.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    12:10:36 am
 */
package com.RYYoutube.Selenium.locatorstratagies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class ByXpathAndOROperator {
	public static void main(String[] args) {
		System.setProperty("webdriver.edge.driver", "D:\\BrowserDriver\\edgedriver_win64\\msedgedriver.exe");
		WebDriver driver= new EdgeDriver();
		driver.manage().window().maximize();
		
		driver.navigate().to("https://demoqa.com/automation-practice-form");
		//And & OR Operator
		
		driver.findElement(By.xpath("//input[@placeholder='First Name' and @id='firstName']")).sendKeys("Sumit");
		
		driver.findElement(By.xpath("//input[@placeholder='Last Name' or @id='lastName4543534']")).sendKeys("Kumar Chaudhary");
	}

}