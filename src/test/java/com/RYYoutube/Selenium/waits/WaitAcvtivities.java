/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 01-Jul-2023
 *  FILE NAME  		: 	 WaitAcvtivities.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    11:34:59 pm
 */
package com.RYYoutube.Selenium.waits;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WaitAcvtivities {
	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		WebDriver driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.navigate().to("https://demoqa.com");
		
		WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(20));
		
		WebElement form=driver.findElement(By.xpath("//div[@class='home-body']//div[2]//div[1]//div[1]"));
		wait.until(ExpectedConditions.elementToBeClickable(form)).click();
		driver.findElement(By.xpath("//span[normalize-space()='Practice Form']")).click();
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Sumit Chaudhary");
	}

}
