/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 21-May-2023
 *  FILE NAME  		: 	 CalendarHandling.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    12:24:56 pm
 */
package com.RYYoutube.Selenium.widgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CalendarHandling {
	public static WebDriver driver;
	public static void main(String[] args) {
		ChromeOptions notification=new ChromeOptions();
		WebDriverManager.chromedriver().setup();
		
		notification.addArguments("--disable-notifications");
		driver =new ChromeDriver(notification);
		driver.navigate().to("https://demoqa.com/date-picker");
		driver.findElement(By.xpath("//input[@id='datePickerMonthYearInput']")).click();
		datePicker("January", "2021", "11");
	}
	
	public static void datePicker(String month, String year, String date) {
		WebElement monthDropDown=driver.findElement(By.xpath("//select[@class='react-datepicker__month-select']"));
		WebElement yearDropDown=driver.findElement(By.xpath("//select[@class='react-datepicker__year-select']"));
		
		Select selectMonth=new Select(monthDropDown);
		selectMonth.selectByVisibleText(month);
		Select selectYear=new Select(yearDropDown);
		selectYear.selectByVisibleText(year);
		
	
		driver.findElement(By.xpath("//*[text()='"+date+"']")).click();
	}

}
