/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-May-2023
 *  FILE NAME  		: 	 CheckboxHandling.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    9:48:24 pm
 */
package com.RYYoutube.Selenium.widgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CheckboxHandling {
	public static void main(String[] args) {
		ChromeOptions notification=new ChromeOptions();
		WebDriverManager.chromedriver().setup();
		
		notification.addArguments("--disable-notifications");
		WebDriver driver =new ChromeDriver(notification);
		driver.navigate().to("https://demoqa.com/checkbox");
		driver.findElement(By.xpath("//button[@title='Toggle']//*[name()='svg']")).click();
		driver.findElement(By.xpath("//li[@class='rct-node rct-node-parent rct-node-expanded']//li[1]//span[1]//button[1]//*[name()='svg']")).click();
		driver.findElement(By.xpath("(//span[@class='rct-checkbox'])[3]")).click();
		driver.findElement(By.xpath("//label[@for='tree-node-commands']//span[@class='rct-checkbox']//*[name()='svg']")).click();
	}

}
