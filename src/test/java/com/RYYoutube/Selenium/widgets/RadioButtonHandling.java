/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-May-2023
 *  FILE NAME  		: 	 RadioButtonHandling.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    9:51:41 pm
 */
package com.RYYoutube.Selenium.widgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class RadioButtonHandling {
	public static void main(String[] args) {
		ChromeOptions notification=new ChromeOptions();
		WebDriverManager.chromedriver().setup();
		
		notification.addArguments("--disable-notifications");
		WebDriver driver =new ChromeDriver(notification);
		driver.navigate().to("https://demoqa.com/radio-button");
		driver.findElement(By.xpath("//label[normalize-space()='Yes']")).click();
		
	}
}
