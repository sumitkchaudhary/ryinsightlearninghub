/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 18-Mar-2023
 *  FILE NAME  		: 	 SelectClassInSelenium.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    8:11:00 am
 */
package com.RYYoutube.Selenium.widgets;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SelectClassInSelenium {
	public static void main(String[] args) {
		WebDriverManager.edgedriver().setup();
		WebDriver driver= new EdgeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/select-menu");
		//Single selection dropdown
		WebElement oldSelectMenu=driver.findElement(By.xpath("//select[@id='oldSelectMenu']"));
		Select selectObject=new Select(oldSelectMenu);
		
		//Select by index
		//selectObject.selectByIndex(9);//We should select magenta
		
		//Select By value
		//selectObject.selectByValue("6");
		
		//Select By visible text
		//selectObject.selectByVisibleText("Purple");
		
		
		List<WebElement> allOptions=selectObject.getOptions();
		
		for(WebElement option : allOptions) {
			System.out.println(option.getText());
		}
		
	}

}
