/**
 * Project Copyright:    R&Y
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 22-Mar-2023
 *  FILE NAME  		: 	 SelectClassMultiSelection.java
 *  PROJECT NAME 	:	 RYYoutubeChannelVideos
 * 	Time			:    6:40:28 am
 */
package com.RYYoutube.Selenium.widgets;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SelectClassMultiSelection {
	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.edgedriver().setup();
		WebDriver driver= new EdgeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/select-menu");
		WebElement mutipleSelection=driver.findElement(By.xpath("//select[@id='cars']"));
		Select selectMultipleValues=new Select(mutipleSelection);
		
		//Check the targated 
		boolean checkMutiOption=selectMultipleValues.isMultiple();
		if(checkMutiOption==true) {
			System.out.println("The targated dropdown is allow to select multiple values");
			//selectMultipleValues.selectByIndex(0);
			//selectMultipleValues.selectByIndex(1);
			
			selectMultipleValues.selectByValue("opel");
			//selectMultipleValues.selectByValue("audi");
			
			selectMultipleValues.selectByVisibleText("Saab");
			selectMultipleValues.selectByVisibleText("Audi");
			
			//WebElement firstSelectedValue=selectMultipleValues.getFirstSelectedOption();
			//System.out.println("First Selected Value: "+firstSelectedValue.getText());
			List<WebElement> getAllSelectedValues=selectMultipleValues.getAllSelectedOptions();
			System.out.println("All selected Values");
			for(WebElement option: getAllSelectedValues) {
				System.out.println(option.getText());
			}
			Thread.sleep(800);
			//selectMultipleValues.deselectByIndex(3);
			selectMultipleValues.deselectAll();
			
			
		}else {
			System.out.println("The targated dropdown is not allow to select multiple values");
		}
	}

}
